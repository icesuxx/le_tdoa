class data:
    def __init__(self):
        self.recvAncList = []
        self.cnvtvAncList = []


class DB:
    def __init__(self):
        self.main_frame = {}

    def setvalue(self, tid, seq):
        if tid in self.main_frame:
            if seq not in self.main_frame[tid]:
                self.main_frame[tid][seq] = data()
        else:
            self.main_frame[tid] = {}
            self.main_frame[tid][seq] = data()

        return self.main_frame[tid][seq]

    def add_valueCnv(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.cnvtvAncList.append(anchor_id) seem
        return len(data.cnvtvAncList)

    def add_valueRecv(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.recvAncList.append(anchor_id)
        return len(data.recvAncList)

    def __str__(self):
        strval = ""
        for t in self.main_frame:
            strval += "tid : " + str(t) + "\n"
            for s in self.main_frame[t]:
                strval += "\tseq: " + str(s) + "\n"
                strval += "\t\trecvAncList : " + str(self.main_frame[t][s].recvAncList) + "\n"
                strval += "\t\tcntvAncList : " + str(self.main_frame[t][s].cnvtvAncList) + "\n"

        return strval


if __name__ == "__main__":
    db = DB()

    valueCount = db.add_valueRecv(1, 1, 'a0')
    print(valueCount)
    valueCount = db.add_valueRecv(1, 2, 'a1')
    print(valueCount)
    valueCount = db.add_valueRecv(1, 1, 'a3')
    print(valueCount)
    valueCount = db.add_valueRecv(1, 2, 'a2')
    print(valueCount)
    valueCount = db.add_valueRecv(2, 1, 'a2')
    print(valueCount)
    valueCount = db.add_valueRecv(2, 2, 'a2')
    print(valueCount)
    valueCount = db.add_valueRecv(2, 2, 'a4')
    print(valueCount)

    print(db)