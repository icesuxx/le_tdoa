#!/usr/bin/python3

import numpy as np
from datetime import datetime
import pandas as pd
import sys

time = datetime.now()  # current date and time
now = time.strftime("%H%M%S.%f")

imu_file_name = '20201026_101132_RTLS_log.csv'

TAG_BLINK_FUNCCODE = "0x83"
TAG_BLINK_FIELD_COUNT = 5

CCP_MASTER_FUNCCODE = "0x84"
CCP_MASTER_FIELD_COUNT = 4

CCP_SLAVE_FUNCCODE = "0x85"
CCP_SLAVE_FIELD_COUNT = 7

DEFAULT_RX_ANTENNA_DELAY = 16418  # 0x4022
DEFAULT_TX_ANTENNA_DELAY = 16418  # 0x4022

headercurr = ['tid', 'seq']
_currTagBlinkSeqNum = pd.DataFrame(columns=headercurr)
headertb = ['aid', 'tid', 'seq']
_tagBlinkReport = pd.DataFrame(columns=headertb)

SOLVE_DIMENSION = 2
SYS_MASTER_ID = 1

def processTokens(line_tokens):
    if len(line_tokens) < 2:
        return

    if line_tokens[0] == TAG_BLINK_FUNCCODE:
        if len(line_tokens) != TAG_BLINK_FIELD_COUNT:
            return

        tid = int(line_tokens[1], 16)
        aid = int(line_tokens[2], 16)
        seq = int(line_tokens[3], 16)
        receivedTimestamp = int(line_tokens[4], 16)
        processTagBlink(now, tid, aid, seq, receivedTimestamp) #0x83

    elif line_tokens[0] == CCP_MASTER_FUNCCODE:
        if len(line_tokens) != CCP_MASTER_FIELD_COUNT:
            return

        mid = int(line_tokens[1], 16)
        seq = int(line_tokens[2], 16)
        masterTxTimestamp = int(line_tokens[3], 16)
        processCCPMaster(now, mid, seq, masterTxTimestamp) #0x84

    elif line_tokens[0] == CCP_SLAVE_FUNCCODE:
        if len(line_tokens) != CCP_SLAVE_FIELD_COUNT:
            return

        mid = int(line_tokens[1], 16)
        aid = int(line_tokens[2], 16)
        seq = int(line_tokens[3], 16)
        masterPrevTimestamp = int(line_tokens[4], 16)
        masterCurrTimestamp = int(line_tokens[5], 16)
        receivedTimestamp2 = int(line_tokens[6], 16)
        processCCPSlave(now, mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp2) #0x85


def processCCPMaster(now, mid, seq, masterTxTimestamp):
    # print("ccp master , " + str(now) + ", " + str(mid) + ", " + str(seq) + ", " + str(masterTxTimestamp))

    # COMPENSATE_ANTENNA_DELAY_FIX
    compensate_masterTxTimestamp = np.subtract(masterTxTimestamp, DEFAULT_TX_ANTENNA_DELAY)
    # print("injectMCCP , " + str(now) + ", "+ str(mid) + ", " + str(seq) + ", m_ts_hex , " + str(hex(compensate_masterTxTimestamp)))

def processTagBlink(now, tid, aid, seq, receivedTimestamp):
    # print("tag blink , " + str(now) + ", " + str(tid) + ", " + str(aid) + ", " + str(seq) + ", " + str(receivedTimestamp))
    print("insertRecvBlinkTS , " + str(now) + ", " + str(tid) + ", " + str(aid) + ", " + str(seq) + ", recv_ts_hex , " + str(hex(receivedTimestamp)))
    #tagBlinkWithSeq = (tid, seq, aid)
    a = 0

class data:
    def __init__(self):
        self.recvAncList = []
        self.cnvtAncList = []


class DB:
    def __init__(self):
        self.main_frame = {}

    def setvalue(self, tid, seq):
        if tid in self.main_frame:
            if seq not in self.main_frame[tid]:
                self.main_frame[tid][seq] = data()
        else:
            self.main_frame[tid] = {}
            self.main_frame[tid][seq] = data()

        return self.main_frame[tid][seq]

    def add_valueCnv(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.cnvtAncList.append(anchor_id)
        return len(data.cnvtAncList)

    def tagBlinkWithSeq(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.recvAncList.append(anchor_id)
        return len(data.recvAncList)


# def tid_seq(aid, tid, seq) :
#     global _currTagBlinkSeqNum
#     global _tagBlinkReport
#     new_tagBlink = {'tid': tid, 'seq': seq, 'aid': aid}
#     if new_tagBlink['tid'] != tid or new_tagBlink['seq'] != seq :
#         _currTagBlinkSeqNum = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         print(_currTagBlinkSeqNum)
#     elif new_tagBlink['tid'] == tid or new_tagBlink['seq'] == seq:
#         _currTagBlinkSeqNum = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         _tagBlinkReport = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         #print(_currTagBlinkSeqNum)
#         print(_tagBlinkReport)

def processCCPSlave(now, mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp2) :
    #print("ccp slave , " + str(now) + ", " + str(mid) + ", " + str(aid) + ", " + str(seq) + ", " + str(masterPrevTimestamp) + ", " + str(masterCurrTimestamp) + ", " + str(receivedTimestamp2))

    #COMPENSATE_ANTENNA_DELAY_FIX
    compensate_masterPrevTimestamp = np.add(masterPrevTimestamp, DEFAULT_TX_ANTENNA_DELAY)
    compensate_masterCurrTimestamp = np.add(masterCurrTimestamp, DEFAULT_TX_ANTENNA_DELAY)
    compensate_receivedTimestamp2 = np.subtract(receivedTimestamp2, DEFAULT_RX_ANTENNA_DELAY)
    #print(compensate_masterPrevTimestamp, compensate_masterCurrTimestamp, compensate_receivedTimestamp2)

    #print("insertCCPDb , " + str(now) + ", "+ str(mid) + ", " + str(aid) + ", " + str(seq) + ", m_ts_hex " + str(hex(compensate_masterCurrTimestamp)) + ", s_ts_hex " + str(hex(compensate_receivedTimestamp2)))


if __name__ == "__main__":

    with open(imu_file_name) as master_file:
        for line in master_file:
            line_tokens = line.split(',')
            processTokens(line_tokens)
            db = DB()
# .(DEFAULT_TX_ANTENNA_DELAY-DEFAULT_RX_ANTENNA_DELAY)
# print(d_anc01.get(), d_anc02.get(),d_anc03.get(), d_anc12.get(),d_anc13.get(), d_anc23.get())

