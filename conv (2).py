#!/usr/bin/python3

import numpy as np
from datetime import datetime
import pandas as pd
import sys


time = datetime.now() # current date and time
now = time.strftime("%H%M%S.%f")

imu_file_name = '20200804_1020_all_anchor.csv'

TAG_BLINK_FUNCCODE = "0x83"
TAG_BLINK_FIELD_COUNT = 19

CCP_MASTER_FUNCCODE = "0x84"
CCP_MASTER_FIELD_COUNT = 4

CCP_SLAVE_FUNCCODE = "0x85"
CCP_SLAVE_FIELD_COUNT = 6

DEFAULT_RX_ANTENNA_DELAY = 16418   #0x4022
DEFAULT_TX_ANTENNA_DELAY = 16418   #0x4022

headercurr = ['tid', 'seq']
_currTagBlinkSeqNum = pd.DataFrame(columns=headercurr)
headertb = ['aid', 'tid', 'seq']
_tagBlinkReport = pd.DataFrame(columns=headertb)

SOLVE_DIMENSION = 2
SYS_MASTER_ID = 1

# def show_entry_fields():
#     print("d_anc#01 %s\nd_anc#02 %s\nd_anc#03 %s\nd_anc#12 %s\nd_anc#13 %s\nd_anc#23" % (d_anc01.get(), d_anc02.get(),d_anc03.get(), d_anc12.get(),d_anc13.get(), d_anc23.get()))
#
# master = tk.Tk()
# tk.Label(master, text="distance between anc#0 and anc#1").grid(row=0)
# tk.Label(master, text="distance between anc#0 and anc#2").grid(row=1)
# tk.Label(master, text="distance between anc#0 and anc#3").grid(row=2)
# tk.Label(master, text="distance between anc#1 and anc#2").grid(row=3)
# tk.Label(master, text="distance between anc#1 and anc#3").grid(row=4)
# tk.Label(master, text="distance between anc#2 and anc#3").grid(row=5)
#
# d_anc01 = tk.Entry(master)
# d_anc02 = tk.Entry(master)
# d_anc03 = tk.Entry(master)
# d_anc12 = tk.Entry(master)
# d_anc13 = tk.Entry(master)
# d_anc23 = tk.Entry(master)
#
# d_anc01.grid(row=0, column=1)
# d_anc02.grid(row=1, column=1)
# d_anc03.grid(row=2, column=1)
# d_anc12.grid(row=3, column=1)
# d_anc13.grid(row=4, column=1)
# d_anc23.grid(row=5, column=1)
#
# tk.Button(master, text='Enter', command=master.quit).grid(row=6, column=0, sticky=tk.W, pady=4)
# tk.mainloop()
# master.mainloop()

def processTokens(line_tokens):
    if len(line_tokens) < 2 : 
        return

    if line_tokens[0] == TAG_BLINK_FUNCCODE :
        if len(line_tokens) != TAG_BLINK_FIELD_COUNT : 
            return
        
        tid = int(line_tokens[1], 16)
        aid = int(line_tokens[2], 16)
        seq = int(line_tokens[3], 16)
        acc_l_x = int(line_tokens[4], 16)
        acc_h_x = int(line_tokens[5], 16)
        acc_l_y = int(line_tokens[6], 16)
        acc_h_y = int(line_tokens[7], 16)
        acc_l_z = int(line_tokens[8], 16)
        acc_h_z = int(line_tokens[9], 16)
        gyr_l_x = int(line_tokens[10], 16)
        gyr_h_x = int(line_tokens[11], 16)
        gyr_l_y = int(line_tokens[12], 16)
        gyr_h_y = int(line_tokens[13], 16)
        gyr_l_z = int(line_tokens[14], 16)
        gyr_h_z = int(line_tokens[15], 16)
        acc_fullscale = int(line_tokens[16],16)
        gyr_fullscale = int(line_tokens[17],16)
        receivedTimestamp = int(line_tokens[18], 16)
        processTagBlink(now, tid, aid, seq, 
                        acc_l_x, acc_h_x, acc_l_y, acc_h_y, acc_l_z, acc_h_z, 
                        gyr_l_x, gyr_h_x, gyr_l_y, gyr_h_y, gyr_l_z, gyr_h_z, 
                        acc_fullscale, gyr_fullscale, receivedTimestamp)

    elif line_tokens[0] == CCP_MASTER_FUNCCODE :
        if len(line_tokens) != CCP_MASTER_FIELD_COUNT : 
            return

        mid = int(line_tokens[1], 16)
        seq = int(line_tokens[2], 16)
        masterTxTimestamp = int(line_tokens[3], 16)
        processCCPMaster(now, mid, seq, masterTxTimestamp)

    elif line_tokens[0] == CCP_SLAVE_FUNCCODE :
        if len(line_tokens) != CCP_SLAVE_FIELD_COUNT : 
            return

        mid = int(line_tokens[1], 16)
        aid = int(line_tokens[2], 16)
        seq = int(line_tokens[3], 16)
        masterPrevTimestamp = int(line_tokens[4], 16)
        masterCurrTimestamp = int(line_tokens[5], 16)
        receivedTimestamp = int(line_tokens[6], 16)
        processCCPSlave(now, mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp)
        
def processCCPMaster(now, mid, seq, masterTxTimestamp) :
    #print("ccp master , " + str(now) + ", " + str(mid) + ", " + str(seq) + ", " + str(masterTxTimestamp))

    #COMPENSATE_ANTENNA_DELAY_FIX
    compensate_masterTxTimestamp = np.subtract(masterTxTimestamp, DEFAULT_TX_ANTENNA_DELAY)

    #print("injectMCCP , " + str(now) + ", "+ str(mid) + ", " + str(seq) + ", m_ts_hex , " + str(hex(compensate_masterTxTimestamp)))


def processTagBlink(now, tid, aid, seq,
                        acc_l_x, acc_h_x, acc_l_y, acc_h_y, acc_l_z, acc_h_z,
                        gyr_l_x, gyr_h_x, gyr_l_y, gyr_h_y, gyr_l_z, gyr_h_z,
                        acc_fullscale, gyr_fullscale, receivedTimestamp) :
    #print("tag blink , " + str(now) + ", " + str(tid) + ", " + str(aid) + ", " + str(seq) + ", "
    #+ str(acc_l_x) + ", " + str(acc_h_x) + ", " + str(acc_l_y) + ", " + str(acc_h_y) + ", " + str(acc_l_z) + ", " + str(acc_h_z) + ", "
    #+ str(gyr_l_x) + ", " + str(gyr_h_x) + ", " + str(gyr_l_y) + ", " + str(gyr_h_y) + ", " + str(gyr_l_z) + ", " + str(gyr_h_z) + ", "
    #+ str(acc_fullscale) + ", " + str(gyr_fullscale) + ", "
    #+ str(receivedTimestamp))

    #print("insertRecvBlinkTS , " + str(now) + ", " + str(tid) + ", " + str(aid) + ", " + str(seq) + ", recv_ts_hex , " + str(hex(receivedTimestamp)))

    tagBlinkWithSeq = (tid, seq, aid)
    a = 0

class data:
    def __init__(self):
        self.recvAncList = []
        self.cnvtAncList = []
class DB:
    def __init__(self):
        self.main_frame = {}

    def setvalue(self, tid, seq):
        if tid in self.main_frame:
            if seq not in self.main_frame[tid]:
                self.main_frame[tid][seq] = data()
        else:
            self.main_frame[tid] = {}
            self.main_frame[tid][seq] = data()

        return self.main_frame[tid][seq]

    def add_valueCnv(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.cnvtAncList.append(anchor_id)
        return len(data.cnvtAncList)

    def tagBlinkWithSeq(self, tid, seq, anchor_id):
        data = self.setvalue(tid, seq)
        data.recvAncList.append(anchor_id)
        return len(data.recvAncList)

# def tid_seq(aid, tid, seq) :
#     global _currTagBlinkSeqNum
#     global _tagBlinkReport
#     new_tagBlink = {'tid': tid, 'seq': seq, 'aid': aid}
#     if new_tagBlink['tid'] != tid or new_tagBlink['seq'] != seq :
#         _currTagBlinkSeqNum = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         print(_currTagBlinkSeqNum)
#     elif new_tagBlink['tid'] == tid or new_tagBlink['seq'] == seq:
#         _currTagBlinkSeqNum = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         _tagBlinkReport = _currTagBlinkSeqNum.append(new_tagBlink, ignore_index = True)
#         #print(_currTagBlinkSeqNum)
#         print(_tagBlinkReport)

def processCCPSlave(now, mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp) :
    #print("ccp slave , " + str(now) + ", " + str(mid) + ", " + str(aid) + ", " + str(seq) + ", " + str(masterPrevTimestamp) + ", " + str(masterCurrTimestamp) + ", " + str(receivedTimestamp))

    #COMPENSATE_ANTENNA_DELAY_FIX
    compensate_masterPrevTimestamp = np.add(masterPrevTimestamp, DEFAULT_TX_ANTENNA_DELAY)
    compensate_masterCurrTimestamp = np.add(masterCurrTimestamp, DEFAULT_TX_ANTENNA_DELAY)
    compensate_receivedTimestamp = np.subtract(receivedTimestamp, DEFAULT_RX_ANTENNA_DELAY)
    #print(compensate_masterPrevTimestamp, compensate_masterCurrTimestamp, compensate_receivedTimestamp)

    #print("insertCCPDb , " + str(now) + ", "+ str(mid) + ", " + str(aid) + ", " + str(seq) + ", m_ts_hex " + str(hex(compensate_masterCurrTimestamp)) + ", s_ts_hex " + str(hex(compensate_receivedTimestamp)))

if __name__ == "__main__" :

    with open(imu_file_name) as master_file :
        for line in master_file :
            line_tokens = line.split(',')
            processTokens(line_tokens)
            db = DB()
#.(DEFAULT_TX_ANTENNA_DELAY-DEFAULT_RX_ANTENNA_DELAY)
# print(d_anc01.get(), d_anc02.get(),d_anc03.get(), d_anc12.get(),d_anc13.get(), d_anc23.get())

